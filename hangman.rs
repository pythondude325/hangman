use std::collections::BTreeSet;
use std::io::Write;
use std::{io, process};

fn process_word(word: &str, used_letters: &BTreeSet<char>) -> Vec<Option<char>> {
    word.chars()
        .map(|c| match used_letters.contains(&c) {
            true => Some(c),
            false => None,
        })
        .collect()
}

fn display_word(letters: &[Option<char>]) {
    println!(
        "\n {}\n",
        letters
            .iter()
            .map(|letter| letter.unwrap_or('_').to_string())
            .collect::<Vec<_>>()
            .join(" ")
    );
}

const STARTING_BAD_GUESSES: usize = 10;

fn main() -> io::Result<()> {
    let mut used_letters = BTreeSet::<char>::new();

    let word = std::str::from_utf8(
        &process::Command::new("/bin/sh")
            .arg("-c")
            .arg("grep -P '^[a-z]+$' /usr/share/dict/words | shuf -n1")
            .output()?
            .stdout,
    )
    .expect("The word was not valid utf-8.")
    .trim_end()
    .to_owned();

    let mut bad_guesses_left = STARTING_BAD_GUESSES;

    display_word(&process_word(&word, &used_letters));

    while bad_guesses_left > 0 {
        print!("Guess a letter: ");
        io::stdout().flush()?;

        let mut guess = String::new();
        io::stdin().read_line(&mut guess)?;
        guess.truncate(guess.trim().len());
        if guess.chars().count() != 1 {
            println!("The guess must only be one letter.");
            continue;
        }
        let guess: char = guess.chars().next().unwrap().to_ascii_lowercase();
        if used_letters.contains(&guess) {
            println!("You've already used that letter.");
            continue;
        }
        used_letters.insert(guess);

        let successful_guess = word.chars().any(|l| l == guess);

        let obscured_word = process_word(&word, &used_letters);
        display_word(&obscured_word);

        if successful_guess {
            // Win condition
            if obscured_word.iter().all(Option::is_some) {
                println!("\u{1f389} You Win! \u{1f389}\n\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}\u{1f44f}");
                return Ok(());
            }
        } else {
            bad_guesses_left -= 1;
            println!(
                "Incorrect. You have {} wrong guesses left.",
                bad_guesses_left
            );
        }

        println!(
            "Letters guessed: {}",
            used_letters
                .iter()
                .map(char::to_string)
                .collect::<Vec<_>>()
                .join(" ")
        );
        println!();
    }

    println!("\u{1f622} You lost. The word was \"{}\".", &word);
    Ok(())
}
